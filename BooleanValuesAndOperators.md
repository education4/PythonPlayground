# Boolean Operators and Values in Python

Why is it that ```not None``` is ```True```, but ```None``` is not equal to ```False```?

## Operators

### ```==```

```==``` is the equivalency operator. It evaluates whether the values of two given arguments are the same.

### ```is```

```is``` is the identity operator. It evaluates whether two given objects are the same object.

### ```not```

```not``` is the boolean not operator. It evaluates a given argument as a boolean then returns the inverse of its value.

### ```and```

```and``` does not return a ```bool```, instead it returns the first argument if the first argument evaluates to ```False```, else it returns the second argument.

```python
>>> emptyList and 5
()
>>> 2 and 0
0
>>> 2 and (5 + 4)
9
```

This works because ```if``` and ```while``` evaluate the given argument as a ```bool``` and, as demonstrated in the below truth table, ```and``` only returns a value that evaluates to ```True``` if both arguments passed evaluate to ```True```.  

|```x```|```y```|```x and y```|
|:-----:|:-----:|:-----------------------:|
|```False```|```False```|```x```|
|```False```|```True```|```x```|
|```True```|```False```|```y```|
|```True```|```True```|```y```|

### ```or```

```or``` operates the same as ```and```, but returns the second argument if the first evaluates to ```False```, else it returns the first argument.

## Values

### ```True``` and ```False```

Python ```True``` and ```False``` are instances of class ```bool```, a subclass of ```int```. ```True``` holds the value of ```1``` and ```False``` the value of ```0```, but are distinct from the objects ```1``` and ```0```.

```python
>>> True.conjugate()
1
>>> False.conjugate()
0
>>> 1 is True
False
>>> 0 is False
False
```

### Non-```bool```s as ```bool```s

Python evaluates all objects as ```True``` unless either the ```__bool__``` or ```__len__``` [data model](https://docs.python.org/3/reference/datamodel.html#basic-customization) methods are defined in the supplied object's class. The ```__bool__``` method takes precedence over the ```__len__``` method, and the objects value is determined by the boolean value of whatever ```__len__``` returns when it is called. A list of available methods can be obtained by calling the ```dir``` method on an object. An objects boolean value is only evaluated when explicitly called for, i.e. with the ```not```, ```if```, ```while```, ```and``` and ```or``` operators or the ```bool``` function. Examples of each below.

## Answer to the Question

```python
if not None == True:
    print("Success!")
```

1. ```if not None == True``` is executed as ```if not (None == True)```
2. ```None == True``` compares the values of the arguments ```None``` and ```True```
3. comparison fails as ```None``` is of class ```NoneType``` and ```True``` is of class ```bool```

```python
 >>> type(True)
<class 'bool'>
>>> type(None)
<class 'NoneType'>
```

4. ```if not None == True``` has become ```if not False```
5. ```not False``` evaluates to ```True``` and the code block is executed

<br />

```python
if None == False:
    print("Success!")
```

1. ```if None == False``` compares the value of the two arguments ```None``` and ```False```
2. comparison fails as ```None``` is of class ```NoneType``` and ```False``` is of class ```Bool```
3. ```if None == False``` has become ```if False```
4. ```print("Success!")``` is passed

<br />

```python
2 == True
```

1. ```2 == True``` compares the value of the two argument ```2``` and ```True```.
2. comparison fails as ```2``` is of class ```int``` and ```True``` is of class ```bool```
3. ```False``` is returned

## Order of Operations

Operators with the same level of precedence are interpreted by Python from left to right in the order they occur within the statement.

1. ```in```, ```not in```, ```is```, ```is not```, ```<```, ```<=```, ```>```, ```>=```, ```<>```, ```!=```, ```==```
2. ```not```
3. ```and```
4. ```or```

In regards to the question, this means ```==``` will execute before ```not```. This can be more clearly seen with the example

```python
>>> not 2 == 3
True
```

If ```not 2``` were to execute first we would expect to see ```False``` as ```not 2``` would evaluate to ```False``` and there would be a type mismatch between ```False``` and ```3```.  
If ```2 == 3``` were to execute first we would expect to see ```True``` as ```2 == 3``` would evaluate to ```False``` since the value of ```2``` and ```3``` are different and ```not False``` evaluates to ```True```.
