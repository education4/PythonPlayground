# What is a Python function?
In Python function is a class of object. This means that functions have attributes and can be passed as an argument, just like any other object. This can be demonstrated by calling the *dir()* data model method on a function. *dir()* lists all the attributes and functions of a given object.
    >>> def myFunction():
    ...     pass
    ...
    >>> dir(myFunction)
    ['__annotations__', '__call__', '__class__', 
    '__closure__', '__code__', '__defaults__', 
    '__delattr__', '__dict__', '__dir__', 
    '__doc__', '__eq__', '__format__', '__ge__', 
    '__get__', '__getattribute__', '__globals__', 
    '__gt__', '__hash__', '__init__', 
    '__init_subclass__', '__kwdefaults__', 
    '__le__', '__lt__', '__module__', '__name__', 
    '__ne__', '__new__', '__qualname__', 
    '__reduce__', '__reduce_ex__', '__repr__', 
    '__setattr__', '__sizeof__', '__str__', 
    '__subclasshook__']

# What is a lambda function?
I had originally thought that a lambda function was just an alternate way of defining a function, limited to a single line and having no name. To check this assumption I quickly wrote two Python scripts, one defining a function using the *def* keyword and the other using the *lambda* keyword. I then disassembled (had the interpreter show me the C functions called) the two scripts.
````
avillalpand3@School:PythonPlayground$ cat regularDefinition.py
def addArguments(x, y):
        return x + y
avillalpand3@School:PythonPlayground$ python3 -m dis regularDefinition.py
  1           0 LOAD_CONST               0 (<code object addArguments at 0x7ff6821b28a0, file "regularDefinition.py", line 1>)
              2 LOAD_CONST               1 ('addArguments')
              4 MAKE_FUNCTION            0
              6 STORE_NAME               0 (addArguments)
              8 LOAD_CONST               2 (None)
             10 RETURN_VALUE

avillalpand3@School:PythonPlayground$ cat lambdaDefinition.py
addArguments = lambda x,y : x + y
avillalpand3@School:PythonPlayground$ python3 -m dis lambdaDefinition.py
  1           0 LOAD_CONST               0 (<code object <lambda> at 0x7f2c60c228a0, file "lambdaDefinition.py", line 1>)
              2 LOAD_CONST               1 ('<lambda>')
              4 MAKE_FUNCTION            0
              6 STORE_NAME               0 (addArguments)
              8 LOAD_CONST               2 (None)
             10 RETURN_VALUE
````
At first glance, it seems I was correct as the function calls are near-identical, but I did notice some differences.
1. Lamda does not accept the *pass* keyword (I had to debug the lambda definition).
2. Lambda functions are truly anonymous, the function is referred to as \<lambda\> rather than by any unique identifier; if you have multiple lambda functions it will be difficult to trace an error.

I decided to research lambda functions further and found this resource: https://realpython.com/python-lambda/  
After some skimming these are my key takeaways on lambda functions:
1. Truly anonymous (bad tracing support)
2. No statements (e.g. pass, return, raise)
3. Only supports a single expression
4. Difficult to use with a variable number of arguments (largely due to 2 and 3)
5. Variable values are bound at function call rather than definition (important for closures)